<?php

namespace Drupal\rsvplist\Controller;

/**
 * Some description about the file.
 *
 * @file
 * Contains Drupal\rsvplist\Controller\ReportController.
 */

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Controller for rsvplist.
 */
class ReportController extends ControllerBase {

  /**
   * Gets all rsvp for all nodes.
   */
  protected function load() {
    $select = Database::getConnection()->select('rsvplist', 'r');
    // Join the user table so we can get the entry creator's username.
    $select->join('users_field_data', 'u', 'r.uid = u.uid');
    // Join the Node table so we can get the events table.
    $select->join('node_field_data', 'n', 'r.nid = n.nid');
    // Select specific fields for the output.
    $select->addField('u', 'name', 'username');
    $select->addField('n', 'title');
    $select->addField('r', 'mail');
    $entries = $select->execute()->fetchAll(\PDO::FETCH_ASSOC);

    // Description about the return.
    return $entries;
  }

  /**
   * Creates the report page.
   */
  public function report() {
    $content = [];
    $content['message'] = [
      '#markup' => StringTranslationTrait::t('Below is a list of events'),
    ];
    $headers = [
      StringTranslationTrait::t('Name'),
      StringTranslationTrait::t('Event'),
      StringTranslationTrait::t('Email'),
    ];
    $rows = [];

    // Substituída
    // foreach ($entries = $this->load() as $entry) {
    // $rows[] = array_map('Drupal\Component\Utility\SafeMarkup::checkPlain',
    // $entry);}.
    foreach ($entries = $this->load() as $entry) {

      $obj_html = new Html();
      // Sanitize each entry.
      $rows[] = array_map(function ($entry) use ($obj_html) {
        // Description about the return.
        return $obj_html::escape($entry);
      }, $entry);
    };

    $content['table'] = [
      '#type' => 'table' ,
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => StringTranslationTrait::t('No entries available'),
    ];
    $content['#cache']['max-age'] = 0;
    // Description about the return.
    return $content;
  }

}
