<?php

namespace Drupal\rsvplist\Form;

/**
 * Short description about the document.
 *
 * @file
 * Contains Drupal\rsvplist\Form\RSVPForm.
 */

use Drupal\Core\Database\Database;
use Drupal\user\Entity\User;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides RSVP Form.
 */
class RSVPForm extends FormBase {

  /**
   * Short description.
   *
   * @inheritDoc
   */
  public function getFormId() {
    return 'rsvplist_email_form';
  }

  /**
   * Short description.
   *
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node = \Drupal::routeMatch()->getParameter('node');
    $nid = NULL;

    if (isset($node)) {

      $nid = $node->id();
    }
    $form['email'] = [
      '#title' => StringTranslationTrait::t("Label do input"),
      '#type' => 'textfield',
      '#size' => 25,
      '#description' => StringTranslationTrait::t("we'll send updates do the email you provide"),
      '#requirement' => 'TRUE',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => StringTranslationTrait::t("Enviar"),
    ];
    $form['nid'] = [
      '#type' => 'hidden',
      '#value' => $nid,
    ];
    // Some return.
    return $form;
  }

  /**
   * Function description.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('email');
    $exploded = explode(".", $value);
    if (
          $value == !\Drupal::service('email.validator')->isValid($value) ||
          in_array('com', $exploded) != 1
      ) {
      $form_state->setErrorByName(
            'email',
            StringTranslationTrait::t(
              '%mail is not a valid email. Please, check and tre again.',
              ['%mail' => $value]
            )
        );
      // Some return.
      return;
    }
    $node = \Drupal::routeMatch()->getParameter('node');
    // Check if email is already set for this node.
    $select = Database::getConnection()->select('rsvplist', 'r');
    $select->fields('r', ['nid']);
    $select->condition('nid', $node->id());
    $select->condition('mail', $value);

    $results = $select->execute();
    if (!empty($results->fetchCol())) {
      // We found a row with this nid and email.
      // Return description.
      return $form_state->setErrorByName('email', StringTranslationTrait::t('The address %mail is already subscribed to this list'), ['%mail' => $value]);
    }

  }

  /**
   * Function description.
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = User::load(\Drupal::currentUser()->id());
    $arr = [
      'mail' => $form_state->getValue('email'),
      'nid' => $form_state->getValue('nid'),
      'uid' => $user->id(),
      'created' => time(),
    ];
    \Drupal::database()->insert('rsvplist')->fields($arr)->execute();
    \Drupal::messenger()->addMessage(StringTranslationTrait::t("Work, work !"));
  }

}
