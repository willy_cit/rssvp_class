<?php

namespace Drupal\rsvplist\Form;

/**
 * @file
 * Contains  Drupal\rsvplist\Form\RSVPSettings.
 */

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Description about the class.
 */
class RSVPSettings extends ConfigFormBase {

  /**
   * Some function.
   */
  public function getFormId() {
    // Returns some string.
    return 'rsvplist_admin_settings';
  }

  /**
   * Som description to the function.
   *
   * @inheritDoc
   */
  public function getEditableConfigNames() {
    // Return an array.
    return [
      'rsvplist.settings',
    ];
  }

  /**
   * Some function.
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $types = node_type_get_names();
    $config = $this->config('rsvplist.settings');
    $form['rsvplist_types'] = [
      "#type" => "checkboxes",
      "#title" => StringTranslationTrait::t('Content types to enable RSVP'),
      "#default_value" => $config->get('allowed_types'),
      "#options" => $types,
      "#description" => StringTranslationTrait::t('Alguma descrição qualquer'),
    ];
    $form['array_filter'] = ["#type" => "value", "#value" => TRUE];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Some function.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $allowed_types = array_filter($form_state->getValue('rsvplist_types'));
    sort($allowed_types);
    $this->config('rsvplist.settings')
      ->set('allowed_types', $allowed_types)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
