<?php

namespace Drupal\rsvplist\Plugin\Block;

/**
 * Short description about the file.
 */
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block.
 *
 * @Block(
 *  id = "rsvp_block",
 *  admin_label = @Translation("RSVP Block")
 * )
 */
class RSVPBlock extends BlockBase implements BlockPluginInterface {

  /**
   * Short description about the function.
   *
   * @inheritDoc
   */
  public function build() {
    // Inline description about the return.
    return \Drupal::formBuilder()->getForm('Drupal\rsvplist\Form\RSVPForm');
  }

  /**
   * Short description about the function.
   *
   * @inheritDoc
   */
  public function blockAccess(AccountInterface $account) {
    $node = \Drupal::routeMatch()->getParameter('node');
    $enabler = \Drupal::service('rsvplist.enabler');
    if ($node) {
      if ($enabler->isEnabled($node)) {
        // Inline description about the return.
        return AccessResult::allowedIfHasPermission($account, 'view rsvplist');
      }
    }
    // Inline description about the return.
    return AccessResult::forbidden();
  }

}
