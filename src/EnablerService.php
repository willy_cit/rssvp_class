<?php

namespace Drupal\rsvplist;

/**
 * Some description about the file.
 */

use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;

/**
 * Some description about the Class.
 */
class EnablerService {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Sets an individual node.
   */
  public function setEnabled(Node $node) {
    if (!$this->isEnabled($node)) {
      $insert = Database::getConnection()->insert('rsvplist_enabled');
      $insert->fields(['nid'], [$node->id()]);
      $insert->execute();
    }
  }

  /**
   * Check if its anabled or no.
   */
  public function isEnabled(Node $node) {
    if ($node->isNew()) {
      // Description about the return.
      return FALSE;
    }
    $select = Database::getConnection()->select('rsvplist_enabled', 're');
    $select->fields('re', ['nid']);
    $select->condition('nid', $node->id());
    $results = $select->execute();
    // Description about the return.
    return !empty($results->fetchCol());
  }

  /**
   * Deletes de enabled settings for an individual node.
   */
  public function delEnabled(Node $node) {
    $delete = Database::getConnection()->delete('rsvplist_enabled');
    $delete->condition('nid', $node->id());
    $delete->execute();
  }

}
